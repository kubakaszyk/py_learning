from preproc import select_features
from sklearn import preprocessing as pp
import numpy as np

def random_forest(x_train, y_train, x_test):
    # Random Forest Prediction
      
    # Import the random forest package
    from sklearn.ensemble import RandomForestClassifier 
    
    x_train = select_features(x_train, [0, 1, 2, 3, 4, 5, 6])
    x_test = select_features(x_test, [0, 1, 2, 3, 4, 5, 6])
        
    # Create the random forest object which will include all the parameters
    # for the fit
    forest = RandomForestClassifier(n_estimators = 100)

    # Fit the training data to the training output and create the decision
    # trees
    forest = forest.fit(x_train,y_train) 

    # Take the same decision trees and run on the test data
    output = forest.predict(x_test)
    return output
    
def linear_regression(x_train, y_train, x_test):
    # Linear Regression - Least Squares

    from sklearn import linear_model
    clf = linear_model.LinearRegression()
    clf.fit(np.array(x_train), np.array(y_train))
    output = clf.predict(np.array(x_test))
    
    return output

def gaussian_naive_bayes(x_train, y_train, x_test):
    # Gaussian Naive Bayes
    
    from sklearn import datasets
    from sklearn.naive_bayes import GaussianNB
    
    #x_train = pp.scl(x_train)
    #x_test = pp.scl(x_test)
    gnb = GaussianNB()
    output = gnb.fit(x_train, y_train).predict(x_test)
    
    return output

def multinomial_naive_bayes(x_train, y_train, x_test):
    # Multinomial Naive Bayes
    
    from sklearn.naive_bayes import MultinomialNB
    clf = MultinomialNB()
    clf.fit(x_train, y_train)
    output = clf.predict(x_test)
    
    return output

def adaboost(x_train, y_train, x_test):
    # Adaboost
    
    x_train = select_features(x_train, [0, 1, 2, 3, 4, 5, 6])
    x_test = select_features(x_test, [0, 1, 2, 3, 4, 5, 6])
    
    from sklearn.cross_validation import cross_val_score
    from sklearn.ensemble import AdaBoostClassifier
    clf = AdaBoostClassifier(n_estimators=5000)
    clf.fit(x_train, y_train)
    output = clf.predict(x_test)     
            
    return output
def lasso_lars(x_train, y_train, x_test):
    
    from sklearn import linear_model
    
    clf = linear_model.LassoLars(alpha=.1)
    clf.fit(x_train, y_train)
    output = clf.predict(x_test)
    
    return output  
    
def svm(x_train, y_train, x_test):
    from sklearn import svm
    clf = svm.SVC()
    clf.fit(x_train, y_train)
    output = clf.predict(x_test)
    
    return output 
    
def kmeans(x):
    from sklearn.cluster import KMeans
    
    out = KMeans(n_clusters=2)
    out.fit(x)
    o = out.predict(x)
    
    return o
