import matplotlib.pyplot as plt
import io as io
import preproc as prp
from sklearn import preprocessing as pp
import numpy as np

def runt():
    train = io.read_csv('../train.csv')
    test = io.read_csv('../test.csv')

    x_train = prp.select_features(train, [2, 4, 5, 6, 7, 9, 10, 11])
    y_train = prp.select_features(train, [1])
    x_test = prp.select_features(test, [1, 3, 4, 5, 6, 8, 9, 10])
    
    x_train = prp.convert_matrix_to_num(x_train)
    y_train = prp.convert_matrix_to_num(y_train)
    x_test = prp.convert_matrix_to_num(x_test)
   
       
    xt = column(x_train, 5)

    plt.scatter(pp.scale(xt), y_train)
    plt.show()
    
def column(matrix, i):
    return [row[i] for row in matrix]
    
def plot(matrix):
    import matplotlib.pyplot as plt
    
    m = convert_matrix_to_num(matrix)
    m = np.array(m)
    m = m.T
    price = (m[0])
    cl = m[7]
    plt.plot(cl, price, 'ro')
    plt.axis([0, 10, 0, 11])
    plt.show()
