from sklearn import preprocessing
import numpy as np

def scl(arr):
    return preprocessing.scale(arr.astype(float))
    
def log_scl(m):
    return np.log(m + 1)
    
def add_kmeans(x):
    import classifiers as c
    
    x = np.append(np.matrix(x), np.matrix(c.kmeans(x)).T, 1)
    
    return x;
    
def select_features(in_matrix, feature_col):
    out = []
    for row in range(len(in_matrix)):
        out2 = []
        for col in range(len(in_matrix[row])):
            if col in feature_col:
                out2.append(in_matrix[row][col])
        out.append(out2)
    return out
   
def convert_to_num(d):
    try:
        return float(d)
    except ValueError:
        return 0
    except TypeError:
        if 'xx.' in d:
            return float(d[3:])
        else:
            return 0
            
def convert_matrix_to_num(d):
    for x in range(len(d)):
        #if type(d[x][0]) is str:
        for y in range(len(d[x])):
            d[x][y] = convert_to_num(d[x][y])
    return d
            
 
def map_gender(m):
    if m == "male":
        return 1
    elif m == "female":
        return 2
    else:
        return 0

def map_port(port):
    if port == "Q":
        return 1
    elif port == "C":
        return 2
    elif port == "S":
        return 3
    else:
        return 0

def map_cabin(cabin):
    if "A" in cabin:
        return 1
    elif "B" in cabin:
        return 2
    elif "C" in cabin:
        return 3
    elif "D" in cabin:
        return 4
    elif "E" in cabin:
        return 5
    elif "F" in cabin:
        return 6
    elif "G" in cabin:
        return 7
    else:
        return 0
    
def map_age(age):
    age = convert_to_num(age)
    r = age/10
    if r > 7:
        return 7
    else:
        return r
        
def map_price(price):
    price = convert_to_num(price)
    r = price/10
    if r > 10:
        return 10
    else:
        return r
