from preproc import *
import inout as io
import classifiers as cl
import numpy as np

def run(alg):
    train = io.read_csv('../train.csv')
    test = io.read_csv('../test.csv')

    x_train = select_features(train, [2, 4, 9])
    y_train = select_features(train, [1])
    x_test = select_features(test, [1, 3, 8])
    
    """for x in range(len(x_train)):    
        x_train[x][1] = map_gender(x_train[x][1])    
        #x_train[x][7] = map_port(x_train[x][7])
        #x_train[x][6] = map_cabin(x_train[x][6])    
        #x_train[x][2] = map_age(x_train[x][2])
        x_train[x][2] = map_price(x_train[x][2])
    
    for x in range(len(x_test)):
        x_test[x][1] = map_gender(x_test[x][1])    
        #x_test[x][7] = map_port(x_test[x][7])
        #x_test[x][6] = map_cabin(x_test[x][6])
        #x_test[x][2] = map_age(x_test[x][2])
        x_test[x][2] = map_price(x_test[x][2])"""
    
    x_train = convert_matrix_to_num(x_train)
    x_test = convert_matrix_to_num(x_test)
    
    x_train = np.matrix(x_train)
    x_test = np.matrix(x_test)
    
    x_train = log_scl(x_train)
    x_test = log_scl(x_test)
    
    x_train = scl(x_train)
    x_test = scl(x_test)
    
     
    if alg == "random_forest":
        output = cl.random_forest(x_train, y_train, x_test)
    elif alg == "linear_regression":
        output = cl.linear_regression(x_train, y_train, x_test)
    elif alg == "gaussian_naive_bayes":
        output = cl.gaussian_naive_bayes(x_train, y_train, x_test)
    elif alg == "multinomial_naive_bayes":
        output = cl.multinomial_naive_bayes(x_train, y_train, x_test)
    elif alg == "adaboost":
        output = cl.adaboost(x_train, y_train, x_test)
    elif alg == "lasso_lars":
        output = cl.lasso_lars(x_train, y_train, x_test)
    elif alg == "svm":
        output = cl.svm(x_train, y_train, x_test)
    else:
        output = cl.random_forest()
    
    output = [test[0::,0], output]
    output = (np.array(output)).T

    return output 
