#ML Tools for non-numerical data
#http://deeplearning.net/software/theano/tutorial/index.html

import csv as csv #http://docs.python.org/2/library/csv.html
import numpy as np
import scipy as scp
import matplotlib as mpl

def read_csv(file_name):
    csv_fo = csv.reader(open(file_name, 'rb'))
    header = csv_fo.next()

    data=[]
    for row in csv_fo:
        data.append(row)
    data = np.array(data)
    
    return data
   
def write_csv(file_name, output_matrix):
    #if type(output_matrix) == "<class 'numpy.matrixlib.defmatrix.matrix'>":
    #    np.savetxt(file_name, output_matrix, delimiter=",")
    #else:
    csv_fo = csv.writer(open(file_name, 'wb'))
    for row in output_matrix:
        csv_fo.writerow(row)
        
    
